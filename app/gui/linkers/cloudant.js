// Load the Cloudant library.
var Cloudant = require('@cloudant/cloudant');

var name = 'a096341c-59ff-4aa0-9e61-facf7f218548-bluemix';
var password = "4ad325d108b4d9c1e3d82f44b508551a7eac4c38d811d777298f873d9593f9c4";

// Initialize the library with my account.
var cloudant = Cloudant({account:name, password:password});

function populate_database_list(){

  cloudant.db.list(function(err, allDbs) {
    console.log('All my databases: %s', allDbs.join(', '))
    var databases = document.getElementById("databases");
    for(var i=0; i<allDbs.length; i++){
      var option = document.createElement("option");
      option.text = allDbs[i];
      option.value = allDbs[i];
      databases.add(option);
    }
  });
}

function download(){

  var selectedDB = document.getElementById("databases").value;
  var db = cloudant.db.use(selectedDB);
  var items = [];

  db.list({include_docs:true}, function (err, body, header) {
    console.log(err);
    console.log(body);
    for(var i=0; i<body.rows.length; i++){
      items.push(body.rows[i].doc.deviceId);
      //console.log(body.rows[i].doc.deviceId);
    }
    document.getElementById("output").innerHTML = items;
  });
  //document.getElementById("output").innerHTML = "For now, imagine a lot of beautiful JSON beign downloaded"
}


/*
  ASK WHICH AND HOW MANY INDEXES ARE USED IN THE BUCKET
  db.index(function(er, result) {
    if (er) {
      throw er;
    }

    console.log('The database has %d indexes', result.indexes.length);
    for (var i = 0; i < result.indexes.length; i++) {
      console.log('  %s (%s): %j', result.indexes[i].name, result.indexes[i].type,
        result.indexes[i].def);
    }

    //result.should.have.a.property('indexes').which.is.an.Array;
    //done();
  });
*/
