function add_species(){
  var new_species = document.getElementById("new_species_name").value.toLowerCase();
  var letters = /^[A-Za-z0-9]+$/;
  var added = false;

  if (new_species.length < 3){
    document.getElementById("output").innerHTML = "Species name must have at least 3 char!"
  }
  else if(!new_species.match(letters)){
    document.getElementById("output").innerHTML = "Please input alphabet characters only"
  }

  else{
    document.getElementById("output").innerHTML = "Added species: "+new_species
    var species_list = document.getElementById("plankton_species");
    var option = document.createElement("option");
    option.text = new_species;
    option.value = new_species;

    //UNEFFICIENT SOLUTION!
    //If it's sorted there is no need for the linear search, dicotomic would be
    //a lot better
    for(var i=0; i<species_list.length; i++){
      if(species_list[i].text>=option.text) {
        if(species_list[i].text==option.text){
          document.getElementById("output").innerHTML = "Species already in the list!"
          added = true;
          break;
        }
        species_list.add(option, species_list[i]);
        added = true;
        break;
      }
    }
    if(added==false){
      species_list.add(option);
    }
    //Write the new list in to the file
    //It's not efficient, it rewrite the whole file every time a new species is added
    //anyway it's ok at the moment. This action won't be very frequent in time
    var species_names = [];
    for (var i=0; i<species_list.length; i++){
      species_names.push(species_list[i].value);
    }

    var fs = require('fs');
    fs.writeFile("plankton_species.txt", species_names.join('\n'), (error) => { /* handle error */ });
  }
}


//TODO
//Still have to write new adds to the file
function populate_species_list(){
  var fs = require("fs");
  var text = fs.readFileSync("plankton_species.txt", "utf-8").split("\n");

  var species_list = document.getElementById("plankton_species");
  for(var i=0; i<text.length-1; i++){
    var option = document.createElement("option");
    option.text = text[i];
    option.value = text[i];
    species_list.add(option);
  }
}


function classify_anomaly(){
  var now = new Date();
  var date = "_"+now.getFullYear()+"-"+(now.getMonth()+1)+"-"+now.getDate()+" "+
  now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
  var image_path = document.getElementById("input_image").files[0].path;
  var image_name = document.getElementById("input_image").files[0].name;
  if(image_name.includes(".jpg")){
    var image_name_classified = document.getElementById("plankton_species").value+date+".jpg"
    document.getElementById("output").innerHTML = image_name+"  classified as  "+image_name_classified
  }
  else if(image_name.includes(".png")){
    var image_name_classified = document.getElementById("plankton_species").value+date+".png"
    document.getElementById("output").innerHTML = image_name+"  classified as  "+image_name_classified
  }
  else {
    document.getElementById("output").innerHTML = "ERROR! Image must be a .png or a .jpg"
  }
  var image_path_classified = image_path.replace(image_name,image_name_classified);
  var fs = require('fs');
  fs.rename(image_path, image_path_classified, function(err) {
      if ( err ) console.log('ERROR: ' + err);
  });
}
