import sys
import time
import json
import cv2
import base64
import numpy as np
import paho.mqtt.client as mqtt
from datetime import datetime

#MQTT paramenters
host='kvcvuf.messaging.internetofthings.ibmcloud.com'
clientid='d:kvcvuf:Monitor:Mon1'
username='use-token-auth'
password='bCmkeIm7Y05bAqkvbE'
topic='iot-2/cmd/+/fmt/json'

def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))

def on_message(client, userdata, msg):
	if msg.topic == 'iot-2/cmd/image/fmt/json':
		print("Image received");
		# converting the json into image back again
		image=json.loads(msg.payload)["data"]

		# ========== DECODER =============================#
		# Convert to base64 encoding and show start of data
		jpg_original = base64.b64decode(image)

		jpg_as_np = np.fromstring(jpg_original,dtype=np.uint8)
		source = cv2.imdecode(jpg_as_np, 1)
		
		decoded_image='mon_repo/image_'+str(datetime.now())+'.jpg'
		
		img2 =cv2.imwrite(decoded_image ,source)

	elif msg.topic == 'iot-2/cmd/area/fmt/json':
		print("Area received");
		with open('mon_repo/area_'+str(datetime.now())+'.json', 'w') as outfile:
			json.dump(msg.payload, outfile)
	
	else:
		print("Unidentified category");

client = mqtt.Client(clientid)
client.username_pw_set(username, password)
client.connect(host, 1883, 60)
client.subscribe(topic)
client.on_connect = on_connect
client.on_message = on_message
client.loop_forever()
