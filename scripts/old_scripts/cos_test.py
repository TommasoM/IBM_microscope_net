import ibm_boto3
from ibm_botocore.client import Config
from ibm_botocore.exceptions import ClientError

# Constants for IBM COS values
COS_ENDPOINT = "https://s3-api.us-geo.objectstorage.service.networklayer.com"
COS_API_KEY_ID = "lODHdeuWkk4KvaYx3nOEEsyBLyuUodWjfGf9DmZW78e7"
COS_AUTH_ENDPOINT = "https://iam.ng.bluemix.net/oidc/token"
COS_SERVICE_CRN = "crn:v1:bluemix:public:cloud-object-storage:global:a/5eee021447a347259c5b34c2f705efa6:a3063955-1c1c-4857-939f-dcbaecca42f3::"
COS_BUCKET_LOCATION = "us-geo"

# Create resource
cos = ibm_boto3.resource("s3",
    ibm_api_key_id=COS_API_KEY_ID,
    ibm_service_instance_id=COS_SERVICE_CRN,
    ibm_auth_endpoint=COS_AUTH_ENDPOINT,
    config=Config(signature_version="oauth"),
    endpoint_url=COS_ENDPOINT
)

def create_bucket(bucket_name):
    print("Creating new bucket: {0}".format(bucket_name))
    try:
        cos.Bucket(bucket_name).create(
            CreateBucketConfiguration={
                "LocationConstraint":COS_BUCKET_LOCATION
            }
        )
        print("Bucket: {0} created!".format(bucket_name))
    except ClientError as be:
        print("CLIENT ERROR: {0}\n".format(be))
    except Exception as e:
        print("Unable to create bucket: {0}".format(e))

create_bucket("bucketTestTom1")


'''
import ibm_boto3
from ibm_botocore.client import Config

api_key = 'lODHdeuWkk4KvaYx3nOEEsyBLyuUodWjfGf9DmZW78e7'
service_instance_id = 'crn:v1:bluemix:public:cloud-object-storage:global:a/5eee021447a347259c5b34c2f705efa6:a3063955-1c1c-4857-939f-dcbaecca42f3:bucket:microscopes-images'
auth_endpoint = 'https://iam.ng.bluemix.net/oidc/token'
service_endpoint = 'https://cos-service.bluemix.net/endpoints'

new_bucket = 'NewBucketTom'
new_cold_bucket = 'NewColdBucketTom'

cos = ibm_boto3.resource('s3',
    ibm_api_key_id=api_key,
    ibm_service_instance_id=service_instance_id,
    ibm_auth_endpoint=auth_endpoint,
    config=Config(signature_version='oauth'),
    endpoint_url=service_endpoint)

cos.create_bucket(Bucket=new_bucket)

cos.create_bucket(Bucket=new_cold_bucket,
    CreateBucketConfiguration={
        'LocationConstraint': 'us-east-1'
    },
)

for bucket in cos.buckets.all():
    print(bucket.name)
'''
