import sys
import time
import json
import ibmiotf.application

options = {
"org": "kvcvuf",
"id": "App1",
"auth-method": "apikey",
"auth-key": "a-kvcvuf-iiz9pwhl7c",
"auth-token": "r6-zwX1P6OGpC3Ik-8",
"clean-session": True
}

sourceDeviceType="Microscope"
sourceDeviceId1="Mic1"
sourceDeviceId2="Mic2"
sourceDeviceId3="Mic3"
sourceImageEvent="image"
sourceAreaEvent="area"

targetDeviceType="Monitor"
targetDeviceId="Mon1"

image_count=0
area_count=0

def EventCallback(event):
	global image_count
	global area_count
	if event.event=="image":
		print("Image received from "+str(event.device));
		image_count=image_count+1
		imageJSON = event.data
		commandData = {'image' : imageJSON}
		print(str(commandData));
		client.publishCommand(targetDeviceType, targetDeviceId, "image", "json", commandData)
	elif event.event=="area":
		print("Area received "+str(area_count));
		area_count=area_count+1
		dataJSON = json.dumps(event.data)
		commandData = {'area' : dataJSON}
		client.publishCommand(targetDeviceType, targetDeviceId, "area", "json", commandData)

client = ibmiotf.application.Client(options)

client.connect()
client.deviceEventCallback = EventCallback

client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId1, sourceImageEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId1, sourceAreaEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId2, sourceImageEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId2, sourceAreaEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId3, sourceImageEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId3, sourceAreaEvent)

while True:
	time.sleep(1)
