#This app saves in foldes everything as JSON files, conversion has to be made yet

import sys
import os
import time
import json
import ibmiotf.application
from threading import Thread
from datetime import datetime

options = {
"org": "kvcvuf",
"id": "App1",
"auth-method": "apikey",
"auth-key": "a-kvcvuf-iiz9pwhl7c",
"auth-token": "r6-zwX1P6OGpC3Ik-8",
"clean-session": True
}

sourceDeviceType="Microscope"
sourceDeviceId1="Mic1"
sourceDeviceId2="Mic2"
sourceDeviceId3="Mic3"
sourceImageEvent="image"
sourceAreaEvent="area"

targetDeviceType="Monitor"
targetDeviceId="Mon1"

image_count=0
area_count=0


def print_error(args):
	print(args[0]);
	

def print_image(device, data):
	directory = 'archive/'+device+'/images/'
	if not os.path.exists(directory):
		os.makedirs(directory)
	with open(directory+"image_"+str(datetime.now())+'.json', 'w') as outfile:
		json.dump(data, outfile)
		
	
def print_area(device, data):
	directory = 'archive/'+device+'/areas/'
	if not os.path.exists(directory):
		os.makedirs(directory)
	with open(directory+'area_'+str(datetime.now())+'.json', 'w') as outfile:
		json.dump(data, outfile)


def EventCallback(event):
	global image_count
	global area_count
	thread = Thread(target = print_error, args = ("Unknown Topic", ));
	if event.event=="image":
		print("Image received from "+str(event.device));
		image_count=image_count+1
		imageJSON = event.data
		thread = Thread(target = print_image, args = (event.device, event.data))
	elif event.event=="area":
		print("Area received "+str(area_count));
		area_count=area_count+1
		dataJSON = json.dumps(event.data)
	else:
		print("Unidentified category");
	thread.start()
	thread.join()


client = ibmiotf.application.Client(options)

client.connect()
client.deviceEventCallback = EventCallback

client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId1, sourceImageEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId1, sourceAreaEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId2, sourceImageEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId2, sourceAreaEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId3, sourceImageEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId3, sourceAreaEvent)

while True:
	time.sleep(1)
