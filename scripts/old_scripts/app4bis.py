# -*- coding: UTF-8 -*-
#This app saves in foldes converting image JSONs into actual JPG images
#It takes more to every thread so it can be cause of thread flooding

import sys
import os
import time
import cv2
import base64
import numpy as np
from ast import literal_eval
import json
import ibmiotf.application
from threading import Thread
from datetime import datetime

options = {
"org": "kvcvuf",
"id": "App1",
"auth-method": "apikey",
"auth-key": "a-kvcvuf-iiz9pwhl7c",
"auth-token": "r6-zwX1P6OGpC3Ik-8",
"clean-session": True
}

sourceDeviceType="Microscope"
sourceDeviceId1="Mic1"
sourceDeviceId2="Mic2"
sourceDeviceId3="Mic3"
sourceImageEvent="image"
sourceAreaEvent="area"
sourceAreaHistogramEvent="area_histogram"

targetDeviceType="Monitor"
targetDeviceId="Mon1"

image_count=0
area_count=0
area_histogram_count=0


def print_error(args):
	print(args[0]);


def store_image(device, imageString):
	directory = 'archive/'+device+'/images/'
	if not os.path.exists(directory):
		os.makedirs(directory)

	# ========== DECODER =============================#
	jpg_original = base64.b64decode(imageString)
	jpg_as_np = np.fromstring(jpg_original,dtype=np.uint8)
	source = cv2.imdecode(jpg_as_np, 1)
	decoded_image = directory+'image_'+str(datetime.now())+'.jpg'
	img2 = cv2.imwrite(decoded_image ,source)


def store_area(device, data):
	directory = 'archive/'+device+'/areas/'
	if not os.path.exists(directory):
		os.makedirs(directory)
	with open(directory+'area_'+str(datetime.now())+'.json', 'w') as outfile:
		json.dump(data, outfile)


def store_area_histogram(device, data):
	directory = 'archive/'+device+'/area_histograms/'
	if not os.path.exists(directory):
		os.makedirs(directory)
	with open(directory+'area_histogram_'+str(datetime.now())+'.json', 'w') as outfile:
		json.dump(data, outfile)


def EventCallback(event):
	global image_count
	global area_count
	global area_histogram_count
	thread = Thread(target = print_error, args = ("Unknown Topic", ));
	if event.event=="image":
		print("Image "+str(image_count)+" received from "+str(event.device));
		print("Name")
		name=event.data['name']
		print(name)
		print("Image")
		imageJSON=event.data['image']
		print(type(imageJSON))
		print(imageJSON)

		'''
		image_count=image_count+1
		imageJSON = json.loads(event.data)
		imageString = str(imageJSON["data"])
		thread = Thread(target = store_image, args = (event.device, imageString))
		'''
	elif event.event=="area":
		print("Area "+str(area_count)+" received from "+str(event.device));
		area_count=area_count+1
		dataJSON = json.dumps(event.data)
		thread = Thread(target = store_area, args = (event.device, dataJSON))
	elif event.event=="area_histogram":
		print("Histogram "+str(area_histogram_count)+" received from "+str(event.device))
		area_histogram_count=area_histogram_count+1
		dataJSON = json.dumps(event.data)
		thread = Thread(target = store_area_histogram, args = (event.device, dataJSON))
	else:
		print("Unidentified topic");
	thread.start()
	thread.join()


client = ibmiotf.application.Client(options)

client.connect()
client.deviceEventCallback = EventCallback

client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId1, sourceImageEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId1, sourceAreaEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId1, sourceAreaHistogramEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId2, sourceImageEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId2, sourceAreaEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId2, sourceAreaHistogramEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId3, sourceImageEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId3, sourceAreaEvent)
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceId3, sourceAreaHistogramEvent)

while True:
	time.sleep(1)
