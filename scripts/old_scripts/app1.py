import sys
import time
import json
import ibmiotf.application
#import paho.mqtt.client as mqtt

options = {
"org": "kvcvuf",
"id": "App1",
"auth-method": "apikey",
"auth-key": "a-kvcvuf-iiz9pwhl7c",
"auth-token": "r6-zwX1P6OGpC3Ik-8",
"clean-session": True
}

sourceDeviceType="Microscope"
sourceDeviceId="Mic1"
sourceImageEvent="image"

targetDeviceType="Monitor"
targetDeviceId="Mon1"	

def EventCallback(event):
	print "Got event " + json.dumps(event.data)
	imageJSON= event.data
	commandData={'data' : imageJSON}
	client.publishCommand(targetDeviceType, targetDeviceId, "image", "json", commandData)

client = ibmiotf.application.Client(options)

client.connect()
client.deviceEventCallback = EventCallback

client.subscribeToDeviceEvents(deviceType=sourceDeviceType, deviceId=sourceDeviceId, event=sourceImageEvent)

while True:
	time.sleep(1)
