#This version don't use the buffers but try to start a different thread for every image to process.
#I don't think this scale very well but it's just for learning threads in python

import sys
import time
import json
import cv2
import base64
import numpy as np
import paho.mqtt.client as mqtt
from threading import Thread
from datetime import datetime

#MQTT paramenters
host='kvcvuf.messaging.internetofthings.ibmcloud.com'
clientid='d:kvcvuf:Monitor:Mon1'
username='use-token-auth'
password='bCmkeIm7Y05bAqkvbE'
topic='iot-2/cmd/+/fmt/json'


def print_error(args):
	print(args[0]);
	

def print_image(args):
	print("--- Creating Image ---");
	with open('mon_repo/image_'+str(datetime.now())+'.json', 'w') as outfile:
		json.dump(args, outfile)
		
	
def print_area(args):
	print("--- Creating Area ---");
	with open('mon_repo/area_'+str(datetime.now())+'.json', 'w') as outfile:
		json.dump(args[0], outfile)
		

def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))


def on_message(client, userdata, msg):
	global pool
	thread = Thread(target = print_error, args = ("Unknown Topic", ));
	if msg.topic == 'iot-2/cmd/image/fmt/json':
		print("Image received: ");
		thread = Thread(target = print_image, args = (msg.payload, ))
	elif msg.topic == 'iot-2/cmd/area/fmt/json':
		print("Area received");
		thread = Thread(target = print_area, args = (msg.payload, ))
	else:
		print("Unidentified category");
	thread.start()
	thread.join()


client = mqtt.Client(clientid)
client.username_pw_set(username, password)
client.connect(host, 1883, 60)
client.subscribe(topic)
client.on_connect = on_connect
client.on_message = on_message
client.loop_forever()
