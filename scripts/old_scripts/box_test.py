# Import two classes from the boxsdk module - Client and OAuth2
from boxsdk import Client, OAuth2
from boxsdk.network.default_network import DefaultNetwork
from pprint import pformat

# Define client ID, client secret, and developer token.
CLIENT_ID = "akvw92eqv7f8yxavlgbhv1va783my1me"
CLIENT_SECRET = "Li5FSjfvqnnr1IMSVA1BtKSupP0O9QEY"
ACCESS_TOKEN = "tDYR7sAxX0hN2eF02w9QZmyxUNcHf4O4"

class LoggingNetwork(DefaultNetwork):
    def request(self, method, url, access_token, **kwargs):
        """ Base class override. Pretty-prints outgoing requests and incoming responses. """
        print '\x1b[36m{} {} {}\x1b[0m'.format(method, url, pformat(kwargs))
        response = super(LoggingNetwork, self).request(
            method, url, access_token, **kwargs
        )
        if response.ok:
            print '\x1b[32m{}\x1b[0m'.format(response.content)
        else:
            print '\x1b[31m{}\n{}\n{}\x1b[0m'.format(
                response.status_code,
                response.headers,
                pformat(response.content),
            )
        return response

# Create OAuth2 object. It's already authenticated, thanks to the developer token.
oauth2 = OAuth2(CLIENT_ID, CLIENT_SECRET, access_token=ACCESS_TOKEN)

# Create the authenticated client
client = Client(oauth2, LoggingNetwork())

# Get information about the logged in user (that's whoever owns the developer token)
my = client.user(user_id='me').get()
print my.name
print my.login
print my.avatar_url
