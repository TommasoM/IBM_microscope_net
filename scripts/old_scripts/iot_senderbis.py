# -*- coding: UTF-8 -*-
import sys
import json
import os
import time
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

imageTopic='iot-2/evt/image/fmt/json'
areaTopic='iot-2/evt/area/fmt/json'
area_histogramTopic='iot-2/evt/area_histogram/fmt/json'
perimTopic='iot-2/evt/perim/fmt/json'

#MQTT default parameters, just for testing
host_default='1kvcvuf.messaging.internetofthings.ibmcloud.com'
clientid_default='1d:kvcvuf:Microscope:Mic1'
username_default='1use-token-auth'
password_default='1rLoqJE7cIOORdyNtTt'


def read_param_from_file():
	global host_default
	global clientid_default
	global username_default
	global password_default
	with open("config_sender.txt") as f:
	    content = f.readlines()
	# you may also want to remove whitespace characters like `\n` at the end of each line
	content = [x.strip() for x in content]
	host_default = content[0]
	clientid_default = content[1]
	username_default = content[2]
	password_default = content[3]


def create_and_connect_default_client():
	client = mqtt.Client(clientid_default)
	client.username_pw_set(username_default, password_default)
	client.connect(host_default, 1883, 60)
	client.loop_start()
	return client


def create_client(clientid, username, password):
	client = mqtt.Client(clientid)
	client.username_pw_set(username, password)
	return client


def connect_to_cloud(client, host):
	client.connect(host, 1883, 60)
	client.loop_start()
	return client


def disconnect_from_cloud(client):
	client.disconnect()


def send_histogram(client, histogram, hist_type):
	if(hist_type=="area_histogram"):
		print("----- SENDING HISTOGRAM -----")
		pub = client.publish(area_histogramTopic, json.dumps(histogram), qos=1)
	pub.wait_for_publish()


def send_image(client, image):
	print("----- SENDING IMAGE -----")
	data = open(image,'r').read()
	pub = client.publish(imageTopic, json.dumps({'name':image, 'image':data}), qos=1)
	pub.wait_for_publish()


def send_area(client, area):
	print("----- SENDING AREA -----")
	data = open(area,'r').read()
	pub = client.publish(areaTopic, json.dumps(data), qos=1)
	pub.wait_for_publish()
