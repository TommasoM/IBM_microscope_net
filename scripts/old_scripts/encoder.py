import cv2
import base64
import numpy as np
import json


# ======== ENCODER ================================#

img = cv2.imread('template_1.jpg',0)
retval, buffer = cv2.imencode('.jpg', img)

# Convert to base64 encoding and show start of data
jpg_as_text = base64.b64encode(buffer)

data={}

with open('template1.json', 'w') as make_file:
    data["data"] = jpg_as_text.decode('ascii')
    final_data = json.dumps(data)
    make_file.write(final_data)
