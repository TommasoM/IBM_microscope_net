# This version saves everything as a JSON without the support of a buffer
# Too many files received will mean to loose some of them

import sys
import time
import json
import cv2
import base64
import numpy as np
import paho.mqtt.client as mqtt
from datetime import datetime

#MQTT paramenters
host='kvcvuf.messaging.internetofthings.ibmcloud.com'
clientid='d:kvcvuf:Monitor:Mon1'
username='use-token-auth'
password='bCmkeIm7Y05bAqkvbE'
topic='iot-2/cmd/+/fmt/json'

image_count=0
area_count=0

def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))

def on_message(client, userdata, msg):
	global image_count
	global area_count
	if msg.topic == 'iot-2/cmd/image/fmt/json':
		print("Image received "+str(image_count));
		image_count=image_count+1
		with open('mon_repo/image_'+str(datetime.now())+'.json', 'w') as outfile:
			json.dump(msg.payload, outfile)

	elif msg.topic == 'iot-2/cmd/area/fmt/json':
		print("Area received "+str(area_count));
		area_count=area_count+1
		with open('mon_repo/area_'+str(datetime.now())+'.json', 'w') as outfile:
			json.dump(msg.payload, outfile)
	
	else:
		print("Unidentified category");
	

client = mqtt.Client(clientid)
client.username_pw_set(username, password)
client.connect(host, 1883, 60)
client.subscribe(topic)
client.on_connect = on_connect
client.on_message = on_message
client.loop_forever()
