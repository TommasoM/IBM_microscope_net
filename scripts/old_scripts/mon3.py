#This version saves everything in buffers. There is a function to print them
#This script is made to test the buffers

import sys
import time
import json
import cv2
import base64
import numpy as np
import paho.mqtt.client as mqtt
from multiprocessing import Pool
from datetime import datetime

#MQTT paramenters
host='kvcvuf.messaging.internetofthings.ibmcloud.com'
clientid='d:kvcvuf:Monitor:Mon1'
username='use-token-auth'
password='bCmkeIm7Y05bAqkvbE'
topic='iot-2/cmd/+/fmt/json'

image_buffer = []
area_buffer = []

image_count=0
area_count=0

def print_buffer ():
	global json_buffer
	print("--- Printing buffers ---");
	
	for i in range(len(image_buffer)):
		print(str(image_buffer[i]));
	print("Image buffer length: "+str(len(image_buffer)));
	
	for i in range(len(area_buffer)):
		print(str(area_buffer[i]));
	print("Area buffer length: "+str(len(area_buffer)));
	
	print("Lengths riepilogue");
	
	print("Area buffer length: "+str(len(area_buffer)));
	print("Image buffer length: "+str(len(image_buffer)));
	

def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))


def write_json(data):
	with open('mon_repo/image_'+str(datetime.now())+'.json', 'w') as outfile:
			json.dump(data, outfile)


def on_message(client, userdata, msg):
	global json_buffer
	global image_count
	global area_count
	if msg.topic == 'iot-2/cmd/image/fmt/json':
		print("Image received "+str(image_count));
		image_count=image_count+1
		image_buffer.append(msg.payload)
		
	elif msg.topic == 'iot-2/cmd/area/fmt/json':
		print("Area received "+str(area_count));
		area_count=area_count+1
		
	else:
		print("Unidentified category");


client = mqtt.Client(clientid)
client.username_pw_set(username, password)
client.connect(host, 1883, 60)
client.subscribe(topic)
client.on_connect = on_connect
client.on_message = on_message
client.loop_forever()
