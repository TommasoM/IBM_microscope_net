import cv2
import base64
import numpy as np
import json
import sys
from datetime import datetime

# ========== DECODER =============================#
with open(sys.argv[1], 'r') as json_data:
    data_dic =json.load(json_data)

# Convert a dic to a string
data_string= str(data_dic["data"])

# Convert to base64 encoding and show start of data
jpg_original = base64.b64decode(data_string)

jpg_as_np = np.fromstring(jpg_original,dtype=np.uint8)
source = cv2.imdecode(jpg_as_np, 1)
img2 =cv2.imwrite("image_"+str(datetime.now())+".jpg",source)
