import cv2
import base64
import sys
import numpy as np
import json
from datetime import datetime

#This code take an image (.jpg) path as parameter (from terminal) and convert it
#into a JSON
# ======== ENCODER ================================#
img = cv2.imread(sys.argv[1],0)
retval, buffer = cv2.imencode('.jpg', img)

# Convert to base64 encoding and show start of data
jpg_as_text = base64.b64encode(buffer)

data={}

#The image is saved with a standard name
with open("encoded_"+str(datetime.now())+".json", 'w') as make_file:
    data["data"] = jpg_as_text.decode('ascii')
    final_data = json.dumps(data)
    make_file.write(final_data)
