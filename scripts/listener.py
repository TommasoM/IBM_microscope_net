# -*- coding: UTF-8 -*-
#This app saves in foldes converting image JSONs into actual JPG images
#It is supposed to be installed on a local "server" machine where all the
#images and histograms are stored real-time
import sys
import os
import time
import cv2
import base64
import json
import numpy as np
from ast import literal_eval
import ibmiotf.application
from threading import Thread
from datetime import datetime

## WARNING: To make this work it's mandatory to register an app on the Watson
#IoT platform and to put here the org name, the app id, auth key and token
options = {
"org": "23rqgm",
"id": "listener",
"auth-method": "apikey",
"auth-key": "a-23rqgm-bipjh38cq5",
"auth-token": "Cse6duWttX6&r_ixMY",
"clean-session": True
}

#When subscribing, you have to specify which event and device you want to
#subscribe to. The "+" means "every", so in this code I'm subscribing to every
#event of every devide of the "Microscope" type
sourceDeviceType="Microscope"
sourceDeviceIdAll="+"
sourceAllEvent="+"

#Just counters for images and histograms. Not really useful
image_count=0
area_histogram_count=0

def print_error(args):
	print(args[0]);

#Whenever an image event is received, it is converted from JSON to jpg and stored.
#There will be a folder for every device and a specific folder for all the images
def store_image(device, data):
	directory = 'archive/'+device+'/images/'
	if not os.path.exists(directory):
		os.makedirs(directory)

	# ========== DECODER =============================#
	jpg_original = base64.b64decode(data["image"])
	jpg_as_np = np.fromstring(jpg_original,dtype=np.uint8)
	source = cv2.imdecode(jpg_as_np, 1)
	decoded_image = directory+str(datetime.now())+'_A'+str(data["area"])+'_R'+str(data["resize"])+'.jpg'
	img2 = cv2.imwrite(decoded_image ,source)

#Whenever there is an area_histogram event, the histogram is saved as a JSON
def store_area_histogram(device, data):
	directory = 'archive/'+device+'/area_histograms/'
	if not os.path.exists(directory):
		os.makedirs(directory)
	with open(directory+'area_histogram_'+str(datetime.now())+'.json', 'w') as outfile:
		json.dump(data, outfile)

#This function is called whenever an event, which the app is subscribed to, occours
def EventCallback(event):
	global image_count
	global area_histogram_count
#To make it work realtime, a new thread is generated whenever there is an event.
#I never tried with a large number publishing many events but a control on the
#number of threads running should be added to avoid thread overflooding problems
	thread = Thread(target = print_error, args = ("Unknown Topic", ));
	if event.event=="image":
		print("Image "+str(image_count)+" received from "+str(event.device));
		image_count=image_count+1
		thread = Thread(target = store_image, args = (event.device, event.data))
	elif event.event=="area_histogram":
		print("Histogram "+str(area_histogram_count)+" received from "+str(event.device))
		area_histogram_count=area_histogram_count+1
		thread = Thread(target = store_area_histogram, args = (event.device, event.data))
	else:
		print("Unidentified topic");
	thread.start()
	thread.join()

#Here I create a client with the applications options setted previously
client = ibmiotf.application.Client(options)
#The client gets connected to the MQTT broker in the Watson IoT platform
client.connect()
#Whenever there is an event of any event this app is subscribet to, this function
#triggers. There are multiple kinds of callbacks, also the status callbeck could
#be interesting
client.deviceEventCallback = EventCallback
#Here is where the subscription actually happen
client.subscribeToDeviceEvents(sourceDeviceType, sourceDeviceIdAll, sourceAllEvent)

while True:
	time.sleep(1)
