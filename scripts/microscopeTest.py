# -*- coding: UTF-8 -*-
#The first thing to do is to import the scipt for the sender
#The iot_sender.py MUST be in the same folder of the script you are using
import iot_sender as sender
import json
#You won't need this library, it's just for testing
import random
import time

#The first thing is to create a client connected to the MQTT broker of the
#IBM IoT cloud platform
client = sender.create_and_connect_client()
i=0
while(True):
#Here I generate a fake histogram with random values
	histogram = {"0":random.randint(600, 800), "1":random.randint(300, 400),
	"2":random.randint(150, 250), "3":random.randint(100, 150), "4":random.randint(80, 110),
	"5":random.randint(40, 70), "6":random.randint(15, 40), "7":random.randint(15, 40),
	"8":random.randint(40, 70), "9":random.randint(15, 40), "10":random.randint(15, 40),
	"11":random.randint(40, 70), "12":random.randint(15, 40), "13":random.randint(15, 40),
	"14":random.randint(40, 70), "15":random.randint(15, 40), "16":random.randint(15, 40),
	"17":random.randint(40, 70), "18":random.randint(15, 40), "19":random.randint(15, 40),
	"width":random.randint(25, 35)}
	i+=1
	#Once every 12 sec I send a small image
	#The image is already saved in a JSON format
	if(i%6==0):
		with open('mic_repo/image_1.json') as f:
			imageJSON = json.load(f)
		sender.send_image(client, imageJSON)
	#Once every 30 sec I send a bigger image
	if(i%15==0):
		with open('mic_repo/image_2.json') as f:
			imageJSON = json.load(f)
		sender.send_image(client, imageJSON)
	#Every 2 seconds, an histogram is sent
	sender.send_histogram(client, histogram, "area_histogram")
	time.sleep(2)

#Last but not least, the disconnection
#The sender will be connected all the time, so the disconnection has to be made only
#when we are closing the microscope application or when we want to close the connection
sender.disconnect_client(client)
print("Disconnected");
#This sleep is to let the time to the sender to disconnect before closing the interpreter
time.sleep(1)
