# Microscope network using IBM Watson IoT

This repository contains:
- The scipt the device need to send data throught the Watson IoT platform
- The researcher desktop application to access stored data and classify it
- The script to run on a local server [optional]

The credentials for the connection to the platform are the one I used on the test platform. They need to be changed according to the user specific credentials.

## 1 - Running the platform
To make the communication possible, two IBM sevices are needed:
- Watson IoT Platform
- Cloudant NoSQL Database

A simple guide on how to deploy and connect those two sevices can be found here:
https://developer.ibm.com/recipes/tutorials/cloudant-nosql-db-as-historian-data-storage-for-ibm-watson-iot-parti/?cm_mc_uid=95418863089615332358428&cm_mc_sid_50200000=51545491536968859598

Once the IoT platform is running, access to the dashboard and go on "Security" and set the security level to "TLS Optional".

The Watson IoT Platform also provides an MQTT broker to allow devices communication.
MQTT is a publish-subscribe light-messaging protocol (IBM, 1999) used mostly for IoT applications.

## 2 - Installing prerequisites

### On the device

OpenCV - image processing. I used Python 2.7
https://www.pyimagesearch.com/2017/09/04/raspbian-stretch-install-opencv-3-python-on-your-raspberry-pi/

Paho MQTT - messaging protocol library
https://www.hivemq.com/blog/mqtt-client-library-paho-python

ibmiotf - Watson IoT platform module
https://github.com/ibm-watson-iot/iot-python

### On the local server (optional)
As a local server, I used a laptop with Ubuntu 16.04 installed.

OpenCV - image processing. I used Python 2.7
https://www.pyimagesearch.com/2017/09/04/raspbian-stretch-install-opencv-3-python-on-your-raspberry-pi/

ibmiotf - Watson IoT platform module
https://github.com/ibm-watson-iot/iot-python

### Researcher PC

TODO

## 3 - Conncecting a device to IoT platform
Now that the IoT platform is running and all the dependencies are installed, it's possible to publish messages so that every device or application subscribed to that specific topic will receive that. A copy of the message will also be stored on the Cloudant database. Both the IoT platform and the Cloudant database are designed for managing JSON files.

To publish, you need to write a configuration file where you put the informations that the device need to connect to the platform.
Inside the folder "script" there is a config_sender.txt file. The information in there need to be updated according to the platform the device has to connect to.
The 4 lines are:
- [orgName].messaging.internetofthings.ibmcloud.com
- d:[orgName]:[DeviceType]:[DeviceID]
- use-token-auth
- [DeviceToken]

The orgName is the one you can see on the top right of your IoT platform dashboard and the DeviceToken is defined when the device is generated on the platform. If the token gets lost, the only possible solution is to delete the device and generate a new one.

This file must be in the same folder as the iot_sender.py.

### iot_sender.py
This Python script need to be included by the device code that's going to send the data throught the MQTT broker, hosted by the Watson IoT platform.
The main functions are:
- create_and_connect_client()
This function read the connection parameters in the config_sender.txt file, create a new client and try to connect it to the platform, according to the read parameters.
If no errors occours, the connected client is returned.
- send_image(client, image)
This function send a plankton image to the broker under the event "image".
The image parameter is not just an image, it's a dictionary with different fields that should look like this:

    {
    "image" : [image converted to string] 
    "area" : [plankton area]
    "resize" : [number of times the image has been resize to fit in 128KB]
    }
    
TODO: Ask Mayara informations about the coding process
- send_histogram(client, histogram, hist_type)
This function send an histogram to the broker. The event is determined by the hist_type. At the moment, the only hist_type available is "area_histogram", but more types can be added in the future.
Also the histogram need to be a dictionary in order to be properly converted to a JSON file and sent to the broker.

At this point, on the IoT platform device page it should be possible to see the connected devices sending data. It's also possible to build some realtime graphs representing the data being sent on the network.
The same data passing throught the broker are stored in the Cloudant database.

## 4 - Using the desktop application
To start the desktop application, navigate to the "app/gui/" folder and, from terminal, execute the command

    npm start

